Name:           wasi-libc
Summary:        WASI libc implementation for WebAssembly

Version:        16
# Use sources from github tags
%global forgeurl        https://github.com/WebAssembly/wasi-libc
%global tag             wasi-sdk-%{version}
%forgemeta
Release:        %autorelease

License:        Apache-2.0 WITH LLVM-exception AND Apache-2.0 AND MIT AND CC0-1.0 AND BSD-2-Clause
URL:            %{forgeurl}
Source:         %{forgesource}

# Although this packages provides binaries, they are not targeted
# for the build platform, but for wasm32-wasi.
# See the relevant discussion (for similar case) here:
# https://www.redhat.com/archives/fedora-devel-list/2009-February/msg02261.html
%global _binaries_in_noarch_packages_terminate_build 0
BuildArch:      noarch

# Some of the library parts are taken from musl
%global         musl_version 1.2.3
Provides:       bundled(musl) = %{musl_version}

BuildRequires:  clang >= 10
BuildRequires:  git
BuildRequires:  llvm >= 10
BuildRequires:  make

%global toolchain   clang
%global build_cflags %{build_cflags} \\\
    --target=wasm32-wasi -m32 -mcpu=generic -mtune=generic \\\
    -fcf-protection=none \\\
    -Wno-error=unused-command-line-argument

# This is a cross-compiling library; use different prefix than usual
%global wasi_prefix %{_prefix}/wasm32-wasi
%global wasi_datadir %{wasi_prefix}/share
%global wasi_includedir %{wasi_prefix}/include
%global wasi_libdir %{wasi_prefix}/lib
%global wasi_make_flags \\\
    THREAD_MODEL=single \\\
    MALLOC_IMPL=dlmalloc \\\
    INSTALL_DIR='%{buildroot}%{wasi_prefix}' \\\
    SYSROOT='%{_builddir}/sysroot'

%description
WASI Libc is a libc for WebAssembly programs built on top of WASI system calls.
It provides a wide array of POSIX-compatible C APIs, including support for
standard I/O, file I/O, filesystem manipulation, memory management, time,
string, environment variables, program startup, and many other APIs.

%prep
%forgeautosetup -S git_am

%build
%make_build %{wasi_make_flags}
# Check defined symbols
# - Generates files that should be installed later.
# - Depends on the flags/compiler version/etc., so failure should not abort build
make %{wasi_make_flags} check-symbols || :

%install
%make_install %{wasi_make_flags}

%check
# Bundled version checks
xargs test '%{musl_version}' = <libc-top-half/musl/VERSION

%files
%doc README.md
%license LICENSE LICENSE-APACHE LICENSE-APACHE-LLVM LICENSE-MIT
%dir %{wasi_prefix}
%{wasi_datadir}
%{wasi_includedir}
%{wasi_libdir}

%changelog
%autochangelog
